/**
 *  Minnie routing that routes with load balancing and compresses SDN tables
 *   Copyright (C) 2015 Myriana RIFAI
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */
package net.beaconcontroller.routing;

/**
 * 
 * @author Myriana RIFAI (mrifai@i3s.unice.fr)
 */
public class Compression {
    
    public native void setTableLimit(int tableLimit);
    // adds the nodes 
    public native void addNodes(long[] nodes);
    // sets the links 
    public native void addLink(long srcNodeID, long dstNodeID, short srcPort, short dstPort, double capacity);
    // to initialize the algorithm object, to call after setTableLimit, addNodes & addLink
    public native void init();
    // deletes a link from the topology
    public native void deleteLink(long srcNodeID, long dstNodeID);
    // returns the route to the destination 
    public native Rule[] calculateRoute(long srcnode, long dstnode, double charge);
    // returns the list of rules that correspond to the links to be turned off 
    public native void turnOffLinks();
    // returns the list of new rules to be added
    public native Rule[] getRules(long node);
    // to call at the end, free memory
    public native void exit();
    // returns true if table of node is full
    public native boolean isFull(long node);
    // compress table of node
    public native void compress(long node);
    // returns the number of rules of node
    public native int getNbRules(long node);
    // returns true the actual number of rules of node
    public native int getActualNbRules(long node);

}
