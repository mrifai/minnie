/**
 *  Minnie routing that routes with load balancing and compresses SDN tables
 *   Copyright (C) 2015 Myriana RIFAI
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */
package net.beaconcontroller.routing;

/**
 * 
 * @author Myriana RIFAI (mrifai@i3s.unice.fr)
 */
public class Rule{
    
    
    Rule(long sw, long matchsrc, long matchdst, short srcPort, short dstPort, long nexthop, int priority) {
        this.sw = sw;
        this.matchsrc = matchsrc;
        this.matchdst = matchdst;
        this.srcPort = srcPort;
        this.dstPort = dstPort;
        this.nexthop = nexthop;
        this.priority = priority;
    }
    
    public String toString() {
        return "switch: "+ sw + ", src:" + matchsrc + ", dst:" + matchdst + ", srcport:" + srcPort + ", dstPort:" + dstPort + ", next:" + nexthop + ", " + priority;
    }
    
    // No star (0), one star (1) , all star (2)
    public int priority;
    // Id switch
    public long sw;
    // Id next switch
    public long nexthop;
    // Entry port
    public short srcPort;
    // Exit port
    public short dstPort;
    // Src
    public long matchsrc;
    // Dest
    public long matchdst;
    
    public native long getSw();
    public native void setSw(long sw);
    
    public native long getNexthop() ;
    public native void setNexthop(long nexthop);
    public native short getSrcPort() ;
    public native void setSrcPort(short srcPort);
    public native short getDstPort();
    public native void setDstPort(short dstPort);
    public native long getMatchsrc();
    public native void setMatchsrc(String matchsrc);
    public native long getMatchdst() ;
    public native void setMatchdst(String matchdst);
    public native long getPriority();
    public native void setPriority(int priority) ;
    
}