/**
 *  Minnie routing that routes with load balancing and compresses SDN tables
 *   Copyright (C) 2015 Myriana RIFAI
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */
package net.beaconcontroller.routing.internal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.beaconcontroller.core.IBeaconProvider;
import net.beaconcontroller.core.IOFMessageListener;
import net.beaconcontroller.core.IOFSwitch;
import net.beaconcontroller.core.IOFSwitchListener;
import net.beaconcontroller.devicemanager.Device;
import net.beaconcontroller.devicemanager.IDeviceManager;
import net.beaconcontroller.devicemanager.IDeviceManagerAware;
import net.beaconcontroller.routing.Compression;
import net.beaconcontroller.routing.Rule;

import org.openflow.protocol.OFFlowMod;
import org.openflow.protocol.OFMatch;
import org.openflow.protocol.OFMessage;
import org.openflow.protocol.OFPacketIn;
import org.openflow.protocol.OFPacketOut;
import org.openflow.protocol.OFPort;
import org.openflow.protocol.OFType;
import org.openflow.protocol.action.OFAction;
import org.openflow.protocol.action.OFActionDataLayer;
import org.openflow.protocol.action.OFActionDataLayerDestination;
import org.openflow.protocol.action.OFActionOutput;
import org.openflow.util.HexString;
import org.openflow.util.U16;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.beaconcontroller.packet.Ethernet;
import net.beaconcontroller.packet.IPv4;
import net.beaconcontroller.topology.ITopology;
import net.beaconcontroller.topology.LinkTuple;

/**
 * 
 * @author Myriana RIFAI (mrifai@i3s.unice.fr)
 */
public class Routing implements IOFMessageListener, IDeviceManagerAware,IOFSwitchListener {
	protected static Logger log = LoggerFactory.getLogger(Routing.class);
	protected IBeaconProvider beaconProvider;
	protected IDeviceManager deviceManager;
	protected HashMap<Long, Integer> rulesnumber;
	private boolean startup;
	static Compression comp = new Compression();
	private int total=0;
	private ArrayList<String> calculatedroutes=new ArrayList<String>();
	private final String DLL_PATH="/home/sdn2/Downloads/beacon-tutorial-1.0.4-restored/src/beacon-1.0.4/net.beaconcontroller.routing/DLL/";
	private final int TABLE_LIMIT=1000;
	private final int LINK_CAPACITY=10000;
	protected ITopology topology;
	private ArrayList<Long> firstlevel=new ArrayList<Long>();
	
	public void startUp() {
		System.load(DLL_PATH+"compression.dll"); 
		startup = true;
		rulesnumber = new HashMap<Long, Integer>();
		beaconProvider.addOFMessageListener(OFType.PACKET_IN, this);
		
	}

	public void shutDown() {
		beaconProvider.removeOFMessageListener(OFType.PACKET_IN, this);
	}

	@Override
	public String getName() {
		return "routing";
	}

	@Override
	public Command receive(IOFSwitch sw, OFMessage msg) throws IOException {
		
		OFPacketIn pi = (OFPacketIn) msg;
		OFMatch match = OFMatch.load(pi.getPacketData(), pi.getInPort());
		
		if (IPv4.fromIPv4Address(match.getNetworkDestination())
				.equalsIgnoreCase("0.0.0.0")
				&& IPv4.fromIPv4Address(match.getNetworkSource())
						.equalsIgnoreCase("0.0.0.0"))
			return Command.CONTINUE;
	
		match.setDataLayerType((short) 0x0800);
		match.setWildcards(OFMatch.OFPFW_ALL
				^ (OFMatch.OFPFW_DL_TYPE | OFMatch.OFPFW_NW_DST_ALL
						| OFMatch.OFPFW_NW_SRC_ALL | OFMatch.OFPFW_NW_DST_MASK
						| OFMatch.OFPFW_NW_SRC_MASK | OFMatch.OFPFW_IN_PORT));

		Device dstDevice = deviceManager.getDeviceByNetworkLayerAddress(match
				.getNetworkDestination());
		Device srcDevice = deviceManager.getDeviceByNetworkLayerAddress(match
				.getNetworkSource());
	
		String route=IPv4.fromIPv4Address(match.getNetworkDestination())+" "+IPv4.fromIPv4Address(match.getNetworkSource());
		log.info(route);
	
		if (comp != null && dstDevice != null && srcDevice!=null )
		 {
			Rule[] rules = comp.calculateRoute(Ethernet.toLong(srcDevice.getDataLayerAddress()),
					Ethernet.toLong(dstDevice.getDataLayerAddress()), 1);
	
			
			if (rules == null  || rules.length==0)
				log.error("no route to host");
			else {
	
				List<OFAction> pktoutactions=new ArrayList<OFAction>();
				Rule pktoutrule=null;
			
				for (int i = 1; i < rules.length; i++) {
					log.info(rules[i].toString());
					if(!calculatedroutes.contains(route))
					total+=1;
					
					if (rulesnumber.containsKey(rules[i].sw))
						rulesnumber.put(rules[i].sw,
								rulesnumber.get(rules[i].sw) + 1);
					else
						rulesnumber.put(rules[i].sw, 1);
					
					OFActionOutput actionsrcdst = new OFActionOutput();
					List<OFAction> actionssrcdst = new ArrayList<OFAction>();
					
					IOFSwitch routesw = beaconProvider.getSwitches().get(
							rules[i].sw);
						match.setInputPort(rules[i].srcPort);

					actionsrcdst.setPort(rules[i].dstPort);

					actionssrcdst.add(actionsrcdst);
					OFActionDataLayer changedestsrcdst = new OFActionDataLayerDestination();
					if (sw.getId()==routesw.getId()) {
						changedestsrcdst.setDataLayerAddress(dstDevice
								.getDataLayerAddress());
					actionssrcdst.add(changedestsrcdst);
						pktoutactions=actionssrcdst;
						pktoutrule=rules[i];
					}
					
					OFFlowMod fmsrcdst = new OFFlowMod()
							.setCommand(OFFlowMod.OFPFC_ADD)
							.setIdleTimeout((short) 0)
							.setBufferId(pi.getBufferId())
							.setHardTimeout((short) 0)
							.setMatch(match)
							.setFlags(OFFlowMod.OFPFF_CHECK_OVERLAP)
							.setPriority((short) (50-rules[i].priority))
							.setActions(actionssrcdst);

					routesw.getOutputStream().write(fmsrcdst);
					
					
						log.info("at "+ System.currentTimeMillis()+ " number of rules="+total);
		 
				if (!calculatedroutes.contains(route)){
				if (comp.getNbRules(rules[i].sw)>=TABLE_LIMIT
						&& !firstlevel.contains(rules[i].sw)){
					log.error("Compression started for switch "+rules[i].sw+ "at "+ System.currentTimeMillis());
						comp.compress(rules[i].sw);
						Rule[] noderules = comp.getRules(rules[i].sw);
						
						OFMatch matchdel = new OFMatch()
						.setWildcards(OFMatch.OFPFW_ALL);
						OFMessage deleteflows = ((OFFlowMod) beaconProvider
						.getSwitches().get(rules[i].sw).getInputStream()
						.getMessageFactory()
						.getMessage(OFType.FLOW_MOD))
						.setMatch(matchdel)
						.setCommand(OFFlowMod.OFPFC_DELETE)
						.setOutPort(OFPort.OFPP_NONE)
						.setLength(U16.t(OFFlowMod.MINIMUM_LENGTH));
						beaconProvider.getSwitches().get(rules[i].sw)
								.getOutputStream().write(deleteflows);
						for (Rule r : noderules) {
							
							OFActionOutput wildcardactionoutput = new OFActionOutput();
							List<OFAction> wildcardactions = new ArrayList<OFAction>();
							wildcardactionoutput.setPort(r.dstPort);
							wildcardactions.add(wildcardactionoutput);
							OFMatch match1 = new OFMatch();
							match1.setDataLayerType((short) 0x0800);
							if (r.srcPort!=-1)
							{
							match1.setInputPort(r.srcPort);
							if (r.matchsrc == -1 && r.matchdst == -1) {
								match1.setWildcards(OFMatch.OFPFW_ALL
										^ (OFMatch.OFPFW_DL_TYPE | OFMatch.OFPFW_IN_PORT));

							} else if (r.matchsrc == -1) {
								match1.setWildcards(OFMatch.OFPFW_ALL
										^ (OFMatch.OFPFW_DL_TYPE
												| OFMatch.OFPFW_NW_DST_ALL
												| OFMatch.OFPFW_NW_DST_MASK | OFMatch.OFPFW_IN_PORT));
								match1.setNetworkDestination((deviceManager
										.getDeviceByDataLayerAddress(Ethernet
												.toMACAddress(r.matchdst)))
										.getNetworkAddresses().iterator()
										.next());
							} else if (r.matchdst == -1) {
								match1.setWildcards(OFMatch.OFPFW_ALL
										^ (OFMatch.OFPFW_DL_TYPE
												| OFMatch.OFPFW_NW_SRC_ALL
												| OFMatch.OFPFW_NW_SRC_MASK | OFMatch.OFPFW_IN_PORT));
								match1.setNetworkSource((deviceManager
										.getDeviceByDataLayerAddress(Ethernet
												.toMACAddress(r.matchsrc)))
										.getNetworkAddresses().iterator()
										.next());
							} else {
								match1.setWildcards(OFMatch.OFPFW_ALL
										^ (OFMatch.OFPFW_DL_TYPE
												| OFMatch.OFPFW_NW_DST_ALL
												| OFMatch.OFPFW_NW_SRC_ALL
												| OFMatch.OFPFW_NW_DST_MASK
												| OFMatch.OFPFW_NW_SRC_MASK | OFMatch.OFPFW_IN_PORT));
								match1.setNetworkDestination((deviceManager
										.getDeviceByDataLayerAddress(Ethernet
												.toMACAddress(r.matchdst)))
										.getNetworkAddresses().iterator()
										.next());
								match1.setNetworkSource((deviceManager
										.getDeviceByDataLayerAddress(Ethernet
												.toMACAddress(r.matchsrc)))
										.getNetworkAddresses().iterator()
										.next());
							}
							}else {
								if (r.matchsrc == -1 && r.matchdst == -1) {
									match1.setWildcards(OFMatch.OFPFW_ALL
											^ (OFMatch.OFPFW_DL_TYPE ));

								} else if (r.matchsrc == -1) {
									match1.setWildcards(OFMatch.OFPFW_ALL
											^ (OFMatch.OFPFW_DL_TYPE
													| OFMatch.OFPFW_NW_DST_ALL
													| OFMatch.OFPFW_NW_DST_MASK));
									match1.setNetworkDestination((deviceManager
											.getDeviceByDataLayerAddress(Ethernet
													.toMACAddress(r.matchdst)))
											.getNetworkAddresses().iterator()
											.next());
								} else if (r.matchdst == -1) {
									match1.setWildcards(OFMatch.OFPFW_ALL
											^ (OFMatch.OFPFW_DL_TYPE
													| OFMatch.OFPFW_NW_SRC_ALL
													| OFMatch.OFPFW_NW_SRC_MASK ));
									match1.setNetworkSource((deviceManager
											.getDeviceByDataLayerAddress(Ethernet
													.toMACAddress(r.matchsrc)))
											.getNetworkAddresses().iterator()
											.next());
								} else {
									match1.setWildcards(OFMatch.OFPFW_ALL
											^ (OFMatch.OFPFW_DL_TYPE
													| OFMatch.OFPFW_NW_DST_ALL
													| OFMatch.OFPFW_NW_SRC_ALL
													| OFMatch.OFPFW_NW_DST_MASK
													| OFMatch.OFPFW_NW_SRC_MASK));
									match1.setNetworkDestination((deviceManager
											.getDeviceByDataLayerAddress(Ethernet
													.toMACAddress(r.matchdst)))
											.getNetworkAddresses().iterator()
											.next());
									match1.setNetworkSource((deviceManager
											.getDeviceByDataLayerAddress(Ethernet
													.toMACAddress(r.matchsrc)))
											.getNetworkAddresses().iterator()
											.next());
								}
							}
							
								OFFlowMod wildcardflowmod = new OFFlowMod()
									.setCommand(OFFlowMod.OFPFC_ADD)
									.setIdleTimeout((short) 0)
									.setHardTimeout((short) 0)
									.setBufferId(pi.getBufferId())
									.setFlags(OFFlowMod.OFPFC_MODIFY_STRICT)
									.setMatch(match1)
									.setPriority((short)(50- r.priority))
									.setActions(wildcardactions);
							
							beaconProvider.getSwitches().get(rules[i].sw)
									.getOutputStream()
									.write(wildcardflowmod);
						}
						
						log.error("Compression ended for switch "+rules[i].sw+ "at "+ System.currentTimeMillis());
				}
					else {
					
					log.error("at "+ System.currentTimeMillis()+ " number of rules="+total);
				}
				}
				}
				if(pktoutactions.size()!=0){
					
				OFPacketOut pktout = new OFPacketOut();
				pktout.setInPort(pktoutrule.srcPort);
				pktout.setActions(pktoutactions);
				pktout.setBufferId(pi.getBufferId());
				pktout.setPacketData(pi.getPacketData());
				sw.getOutputStream().write(pktout);
				}
			}
		}
		return Command.CONTINUE;
	}

	private void initializeMinnie() {
		if (startup && deviceManager.getDevices().size() ==128 && beaconProvider.getSwitches().size()==36) {

			Map<LinkTuple, Long> links = topology.getLinks();
	
			Long[] nodes = beaconProvider.getSwitches().keySet().toArray(new Long[0]);
			for( Long s:beaconProvider.getSwitches().keySet())
			log.info("switch dpid="+HexString.toHexString(s));
			long[] lnodes = new long[nodes.length
					+ deviceManager.getDevices().size()];
			for (int i = 0; i < nodes.length; i++) {
				lnodes[i] = nodes[i];
			}
			int i = nodes.length;
			for (Device x : deviceManager.getDevices()) {
				lnodes[i] = Ethernet.toLong(x.getDataLayerAddress());
				i++;
				if (!firstlevel.contains(x.getSw().getId()))
					firstlevel.add(x.getSw().getId());
			}
			comp.addNodes(lnodes);
			comp.setTableLimit(TABLE_LIMIT);

			for (LinkTuple l : links.keySet()) {

				comp.addLink(l.getSrc().getSw().getId(), l.getDst().getSw()
						.getId(), l.getSrc().getPort(), l.getDst().getPort(),
						LINK_CAPACITY);
				comp.addLink(l.getDst().getSw().getId(), l.getSrc().getSw()
						.getId(), l.getDst().getPort(), l.getSrc().getPort(),
						LINK_CAPACITY);

			}
			i = nodes.length;
			for (Device x : deviceManager.getDevices()) {
				comp.addLink(Ethernet.toLong(x.getDataLayerAddress()), x
						.getSw().getId(), (short) i, x.getSwPort(), LINK_CAPACITY);
				comp.addLink(x.getSw().getId(),
						Ethernet.toLong(x.getDataLayerAddress()),
						x.getSwPort(), (short) i, LINK_CAPACITY); 

				i++;
			}
			comp.init();
			startup = false;
		}
	}

	/**
	 * @param beaconProvider
	 *            the beaconProvider to set
	 */
	public void setBeaconProvider(IBeaconProvider beaconProvider) {
		this.beaconProvider = beaconProvider;
	}

	/**
	 * @param deviceManager
	 *            the deviceManager to set
	 */
	public void setDeviceManager(IDeviceManager deviceManager) {
		this.deviceManager = deviceManager;
	}

	public void setTopology(ITopology topology) {
		this.topology = topology;
	}
	
	@Override
	public void deviceAdded(Device device) {
		initializeMinnie();
	}

	@Override
	public void deviceRemoved(Device device) {
		// TODO Auto-generated method stub
	}

	@Override
	public void deviceNetworkAddressAdded(Device device,
			Set<Integer> networkAddresses, Integer networkAddress) {
		// TODO Auto-generated method stub
	}

	@Override
	public void deviceNetworkAddressRemoved(Device device,
			Set<Integer> networkAddresses, Integer networkAddress) {
		// TODO Auto-generated method stub
	}

	@Override
	public void deviceMoved(Device device, IOFSwitch oldSw, Short oldPort,
			IOFSwitch sw, Short port) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addedSwitch(IOFSwitch sw) {
		initializeMinnie();
		
	}

	@Override
	public void removedSwitch(IOFSwitch sw) {
		// TODO Auto-generated method stub
		
	}
}
